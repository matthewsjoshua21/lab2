﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Lab2
{   //Techno Starfish
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            fillthelist();

        }

        public void fillthelist()
        {
            var items = new ObservableCollection<ImageSource>();
            for (int i = 0; i < 50; i++)
            {
                items.Add("guitar.jpg");
                ListV.ItemsSource = items;
            }
        }
    }
}
